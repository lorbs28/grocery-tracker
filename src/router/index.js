import Vue from 'vue'
import Router from 'vue-router'

import CurrentGroceryListContainer from '@/components/CurrentGroceryList/CurrentGroceryListContainer'
import AddGroceryListItemContainer from '@/components/CurrentGroceryList/AddGroceryListItemContainer'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'CurrentGroceryListContainer',
      component: CurrentGroceryListContainer
    },
    {
      path: '/item/add',
      name: 'AddGroceryListItemContainer',
      component: AddGroceryListItemContainer
    }
  ]
})
